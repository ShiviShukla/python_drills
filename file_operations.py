"""
Implement the below file operations.
Close the file after each operation.
"""


def read_file(path):
    f_obj = open(path, 'r')
    read_data = f_obj.read()
    f_obj.close()
    return read_data


def write_to_file(path, s):
    f_obj = open(path, 'w')
    f_obj.write(s)
    f_obj.close()

    f_obj = open(path, 'r')
    read_data = f_obj.read()
    f_obj.close()
    return read_data


def append_to_file(path, s):
    f_obj = open(path, 'a')
    f_obj.write(s)
    f_obj.close()

    f_obj = open(path, 'r')
    read_data = f_obj.read()
    f_obj.close()
    return read_data



def numbers_and_squares(n, file_path):
    """
    Save the first `n` natural numbers and their squares into a file in the csv format.

    Example file content for `n=3`:

    1,1
    2,4
    3,9
    """
    with open(file_path, 'w') as f:
        for i in range(1, n + 1):
            f.write("{0}{1}{2}{3}".format(i, ',', i * i, '\n'))

    file_read = open(file_path, 'r')
    data = file_read.read()
    file_read.close()
    return data
