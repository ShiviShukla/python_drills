

# file_op()

def read_file(path):
    f_obj = open(path, 'r')
    read_data = f_obj.read()
    f_obj.close()
    return read_data


def write_to_file(path, s):
    f_obj = open(path, 'w')
    f_obj.write(s)
    f_obj.close()

    f_obj = open(path, 'r')
    read_data = f_obj.read()
    f_obj.close()
    return read_data


def append_to_file(path, s):
    f_obj = open(path, 'a')
    f_obj.write(s)
    f_obj.close()

    f_obj = open(path, 'r')
    read_data = f_obj.read()
    f_obj.close()
    return read_data

path = '/home/shivi/prac.txt'
s= '\n hi i am new text'
print(append_to_file(path, s))