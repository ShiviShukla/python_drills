def integers_from_start_to_end_using_range(start, end, step):
    input_list = []
    for item in range(start, end, step):
        input_list.append(item)
    return input_list
    # """return a list"""

def integers_from_start_to_end_using_while(start, end, step):
    input_list = []
    if start < end:
        while (start < end):
            input_list.append(start)
            start += step
    else:
        while (start > end):
            input_list.append(start)
            start += step
    return input_list



def two_digit_primes():
    """
    Return a list of all two-digit-primes
    """
    num = 10
    prime_list = list()
    while len(str(num)) <= 2:
        prime_flag = True
        for i in range(2, num):
            if num % i == 0:
                prime_flag = False
                break
        if prime_flag:
            prime_list.append(num)
        num += 1
    return prime_list