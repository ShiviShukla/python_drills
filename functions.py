def is_prime(n):
    """
    Check whether a number is prime or not
    """
    if n%2==0:
        return True
    else:
        return False


def n_digit_primes(digit = 2):
    """
    Return a list of `n_digit` primes using the `is_prime` function.

    Set the default value of the `digit` argument to 2
    """
    prime_list = []
    for num in range(2, digit+1):
        if is_prime(num):
            prime_list.append(num)

