def word_count(s):
    s= s.replace(',', '')
    s = s.replace('.', '')
    word_list = str(s).split(" ")
    word_count_dict = dict()
    for word in word_list:
        if word in word_count_dict.keys():
            word_count_dict[word]+=1
        else:
            word_count_dict[word] = 1
    return word_count_dict
    """
    Find the number of occurrences of each word
    in a string(Don't consider punctuation characters)
    """

def dict_items(d):
    output_list = list()

    for key in d.keys():
        output_list.append((key,d[key]))
    return output_list
    """
    Return a list of all the items - (key, value) pair tuples - of the dictionary, `d`
    """


def dict_items_sorted(d):
    output_list = list()

    for key in sorted(d.keys()):
        output_list.append((key, d[key]))
    return output_list


    """
    Return a list of all the items - (key, value) pair tuples - of the dictionary, `d`
    sorted by `d`'s keys
    """
