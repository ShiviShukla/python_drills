import copy

def sum_items_in_list(x):
    """

    :param x: type: list
    :return: sum
    """
    return sum(x)

def list_length(x):
    """

    :param x: type;list
    :return: length
    """
    return len(x)


def last_three_items(x):
    """

    :param x: type;list
    :return:
    """
    if len(x) > 3:
        return x[len(x)-3 : len(x)]
    else:
        return x



def first_three_items(x):
    """

    :param x: type;list
    :return:
    """
    if len(x) > 3:
        return x[0: 3]
    else:
        return x


def sort_list(x):
    """

    :param x:
    :return:
    """
    return sorted(x)


def append_item(x, item):
    """

    :param x: type; list
    :param item: type; number
    :return:
    """
    x.append(item)
    return x

def remove_last_item(x):
    """

    :param x: type: list
    :return: list
    """
    x.pop()
    return x


def count_occurrences(x, item):
    """

    :param x: type; list
    :param item: type; number
    :return: list
    """
    return x.count(item)

def is_item_present_in_list(x, item):
    """

    :param x: type; list
    :param item: number
    :return: boolean
    """
    if item in x:
        return True
    else:
        return False


def append_all_items_of_y_to_x(x, y):
    """
    x and y are lists
    """
    x.extend(y)
    return x


def list_copy(x):
    """
    Create a shallow copy of x
    """
    # shallow_copy = x.
    x_copy = copy.copy(x)
    return x_copy